import DS from 'ember-data';
import { pluralize } from 'ember-inflector';
import { underscore } from '@ember/string';

export default DS.JSONAPIAdapter.extend({
  pathForType (type) {
    return pluralize(underscore(type));
  },

  shouldReloadRecord () {
    return false;
  },

  shouldBackgroundReloadRecord (store, snapshot) {
    console.log("Calling shouldBackgroundReloadRecord");
    const loadedAt = snapshot.record.get('loadedAt');

    // if it was loaded more than an hour ago
    if (Date.now() - loadedAt > 10000) {
      return true;
    } else {
      return false;
    }
  }
});
