# bookstore

Simple example of interaction Ember.js application and Rails backend
through JSON api.

## Installation

* `git clone <repository-url>` this repository
* `cd bookstore`
* `npm install`

## Running / Development

* `ember server --proxy http://localhost:3000`
* Visit your app at [http://localhost:4200](http://localhost:4200).
